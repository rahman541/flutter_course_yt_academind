import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  final List<String> products;

  Products([this.products = const []]) {
    print('[Product Widget] Construtor');
  }

  @override
  Widget build(BuildContext context) {
    print('[Product Widget] build()');
    return Column(
        children: products
            .map((el) => Card(
                    child: Column(children: <Widget>[
                  Image.asset('assets/food.jpeg'),
                  Text(el)
                ])))
            .toList());
  }
}
